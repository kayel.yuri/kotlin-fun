package model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Asset(val path: String, val name: String) : Parcelable {
    
    val fullpath get() = "$path/$name"
    
    val extension get() = name.substringAfter(".")
    
    override fun toString() = fullpath
}