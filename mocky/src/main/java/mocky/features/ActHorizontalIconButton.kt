package mocky.features

import android.os.Bundle
import android.widget.Toast
import base.ActBase
import component.IconButton
import kotlinx.android.synthetic.main.act_horizontal_icon_button.*
import kotlinfun.mocky.R

class ActHorizontalIconButton : ActBase(R.layout.act_horizontal_icon_button) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setOnClick()
    }

    private fun setOnClick() {
        val btnList = btn_list
        for (i in 0 until btnList.childCount) {
            val childAt = btnList.getChildAt(i)
            if (childAt is IconButton)
                childAt.setOnClickListener {
                    Toast.makeText(this, childAt.getText(), Toast.LENGTH_LONG).show()
                }
        }
    }
}
