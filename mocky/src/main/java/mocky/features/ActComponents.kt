package mocky.features

import android.os.Bundle
import base.ActBase
import extension.addViews
import extension.newRow
import extension.newText
import extension.onClickStart
import kotlinx.android.synthetic.main.act_components.*
import kotlinx.android.synthetic.main.act_share_view.*
import kotlinfun.mocky.R

class ActComponents : ActBase(R.layout.act_components) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        btn_icon_horizontal.onClickStart(ActHorizontalIconButton::class)
        btn_proof.onClickStart(ActReceipt::class)
        btn_fun_button.onClickStart(ActFunButton::class)
        btn_fun_checkbox.onClickStart(ActFunCheckBox::class)
        btn_label_value.onClickStart(ActLabelView::class)
        btn_fullscreen_dialog.onClickStart(ActFullScreenDialog::class)
        btn_styles_textview.onClickStart(ActStylesTextView::class)
        btn_menu_button.onClickStart(ActMenuButton::class)
        btn_linear_toolbar.onClickStart(ActLinearToolbar::class)
        btn_share_view.onClickStart(ActShareView::class)
        btn_loading_button.onClickStart(ActLoadingOnButton::class)
        btn_scroll_animation.onClickStart(ActScrollAnimation::class)
    }
}

class ActStylesTextView : ActBase(R.layout.act_styles_textview)

class ActShare : ActBase(R.layout.act_save_share)

class ActFunCheckBox : ActBase(R.layout.act_fun_checkbox)

class ActLabelView : ActBase(R.layout.act_label_view)

class ActMenuButton : ActBase(R.layout.act_menu_button)

class ActLinearToolbar : ActBase(R.layout.act_linear_toolbar)

class ActShareView : ActBase(R.layout.act_share_view) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        label_share_view_list.run {
            addViews(
                newText("Rafinha"),
                newText("404"),
                newText("Henrique"),
                newRow(newText("Rafinha"), newText("404"), newText("Henrique"))
            )
        }
    }
}

