package mocky.features

import android.os.Handler
import android.view.ViewGroup
import base.ActBase
import component.LoadingButton
import kotlinfun.mocky.R

class ActLoadingOnButton : ActBase(R.layout.act_loading_on_button) {

    override fun ViewGroup.onView() {
        val loadingButton = findViewById<LoadingButton>(R.id.loading_button)
        val loadingButton2 = findViewById<LoadingButton>(R.id.loading_button2)
        val loadingButton3 = findViewById<LoadingButton>(R.id.loading_button3)

        loadingButton.set3secondsLoading()
        loadingButton2.set3secondsLoading()
        loadingButton3.set3secondsLoading()

    }
    private fun LoadingButton.set3secondsLoading() {
        setOnClickListener {
            setLoadingState(true)
            Handler().postDelayed({ setLoadingState(false) }, 3000) }
    }
}