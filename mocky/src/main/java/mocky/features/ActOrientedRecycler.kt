package mocky.features

import base.ActBind
import component.ItemViewBuilder
import component.setup
import extension.get
import extension.onClick
import extension.toast
import component.viewBind
import kotlinfun.mocky.databinding.ActOrientedRecyclerBinding
import kotlinfun.mocky.databinding.WhiskasSacheBinding

class ActOrientedRecycler : ActBind<ActOrientedRecyclerBinding>() {

    override val binding: ActOrientedRecyclerBinding by viewBind()

    override fun ActOrientedRecyclerBinding.onBoundView() {
        orientedRecycler.setup<ItemViewWhiskas>(listOf(1..999))
    }
}

class ItemViewWhiskas : ItemViewBuilder<Int, WhiskasSacheBinding>() {

    override val binding: WhiskasSacheBinding by viewBind()

    override fun WhiskasSacheBinding.onBind(position: Int) {
        val number = collection.get(position)

        whiskasText.run {
            text = number.toString()
            onClick { toast(number) }
        }
    }
}