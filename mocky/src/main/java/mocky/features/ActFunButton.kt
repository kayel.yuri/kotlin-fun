package mocky.features

import android.os.Bundle
import android.widget.Toast.LENGTH_SHORT
import android.widget.Toast.makeText
import component.BaseFunButton
import kotlinx.android.synthetic.main.act_fun_button.btn_list
import kotlinfun.mocky.R
import base.ActBase

class ActFunButton : ActBase(R.layout.act_fun_button) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setOnClick()
    }

    private fun setOnClick() {
        btn_list.run {
            for (index in 0 until childCount) {
                getChildAt(index).run {
                    if (this is BaseFunButton) {
                        setOnClickListener { makeText(this@ActFunButton, text, LENGTH_SHORT).show() }
                    }
                }
            }
        }
    }
}
