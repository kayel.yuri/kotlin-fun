package mocky.features

import android.view.ViewGroup
import androidx.core.widget.NestedScrollView
import base.ActBase
import extension.dp
import extension.scaleViewOnScroll
import kotlinfun.mocky.R

class ActScrollAnimation : ActBase(R.layout.scroll_animation_example) {

    override fun ViewGroup.onView() {
        val nestedScrollView = findViewById<NestedScrollView>(R.id.nested_scroll)

        nestedScrollView.scaleViewOnScroll(
            view = findViewById(R.id.image_scroll),
            maxHeight = 200.dp,
            minHeight = 100.dp,
            scrollSize = 100.dp,
            shouldFade = true
        )
    }
}
