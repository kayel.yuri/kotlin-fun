package mocky

import android.os.Bundle
import androidx.activity.viewModels
import base.ActBase
import extension.onClickStart
import kotlinfun.mocky.R
import kotlinx.android.synthetic.main.act_main.*
import mocky.features.ActComponents

class ActMain : ActBase(R.layout.act_main) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        act_main_button_components.onClickStart(ActComponents::class)
    }
}