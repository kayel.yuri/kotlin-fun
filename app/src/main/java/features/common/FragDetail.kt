package features.common

import base.FragBind
import kotlinfun.app.databinding.FragDetailBinding
import component.viewBind
import extension.readAsset

class FragDetail : FragBind<FragDetailBinding>() {

    override val binding: FragDetailBinding by viewBind()

    override fun FragDetailBinding.onBoundView() {
        codeView.setup {
            style = ANDROIDSTUDIO
            code = arguments?.getString("asset") ?: context.readAsset("base/IPermissionResult.kt")
        }
    }
}