package features.home

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import base.FragBase
import component.CodeView
import extension.readAsset
import kotlinfun.app.R

class FragDashboard : FragBase() {

    override val contentView by lazy { ViewPager(requireContext()) }
    private val assets = mutableListOf("")
    private val views = mutableListOf<View>()

    override fun Bundle.onArguments() {
        assets.addAll(getStringArrayList("assets") ?: mutableListOf("Veio vazio"))
    }

    override fun ViewGroup.onView() {
     
        
        contentView.adapter = TestPagerAdapter(views)
    }

}

class TestPagerAdapter(
    private val views: List<View>,
    private val nomes: List<String> = listOf()
) : PagerAdapter() {

    override fun getPageTitle(position: Int) = nomes[position]

    override fun getCount() = views.size

    override fun instantiateItem(container: ViewGroup, position: Int) =
        views[position].apply {
            container.addView(this, 0)
        }

    override fun isViewFromObject(view: View, objekt: Any) = view == objekt

    override fun destroyItem(container: ViewGroup, position: Int, objekt: Any) =
        container.removeViewAt(position)
}