package features.home

import android.os.Bundle
import android.view.ViewGroup
import base.FragBase
import component.ItemViewBuilder
import component.recyclerAdapter
import component.VerticalRecycler
import component.viewBind
import extension.get
import extension.readAsset
import kotlinfun.app.databinding.ItemCodeBinding

class FragViewImplementation : FragBase() {
    
    override val contentView by lazy { VerticalRecycler(requireContext()) }
    private val assets = mutableListOf<String>()
    
    override fun Bundle.onArguments() {
        assets.addAll(getStringArrayList("assets") ?: mutableListOf("Veio vazio"))
    }
    
    override fun ViewGroup.onView() {
        contentView.adapter = recyclerAdapter<ItemCodeBuilder>(assets)
    }
    
}

class ItemCodeBuilder : ItemViewBuilder<String, ItemCodeBinding>() {
    
    override val binding: ItemCodeBinding by viewBind()
    
    override fun ItemCodeBinding.onBind(position: Int) {
        itemCodeview.setup {
            code = context.readAsset(collection.get(position))
        }
    }
    
}