package features.home

import android.app.Activity
import android.view.ViewGroup
import features.base.ActAppBase
import base.*
import component.ItemViewBuilder
import component.VerticalGridRecycler
import component.setup
import component.viewBind
import extension.*
import kotlinfun.app.databinding.ActHostItemBinding
import kotlinfun.app.R
import kotlin.reflect.KClass

class FragHome : FragBase() {

    override val contentView by lazy { VerticalGridRecycler(requireContext(), spanCount = 2) }

    override fun ViewGroup.onView() {
        contentView.setup<ItemViewHome>(
            listOf(
                ItemHome(
                    "Base",
                    R.drawable.ic_launcher_foreground,
                    ActAppBase::class
                ),
                ItemHome(
                    "Views",
                    R.drawable.ic_launcher_foreground,
                    ActSample::class
                ),
                ItemHome(
                    "Test",
                    R.drawable.ic_launcher_foreground,
                    ActSample::class
                ),
                ItemHome(
                    "Fragment",
                    R.drawable.ic_launcher_foreground,
                    ActSample::class
                ),
                ItemHome(
                    "Toast",
                    R.drawable.ic_launcher_foreground,
                    ActSample::class
                ),
                ItemHome(
                    "Dialog",
                    R.drawable.ic_launcher_foreground,
                    ActSample::class
                ),
                ItemHome(
                    "Activity",
                    R.drawable.ic_launcher_foreground,
                    ActSample::class
                ),
                ItemHome(
                    "Views",
                    R.drawable.ic_launcher_foreground,
                    ActSample::class
                ),
                ItemHome(
                    "Intent",
                    R.drawable.ic_launcher_foreground,
                    ActSample::class
                ),
                ItemHome(
                    "Fragment",
                    R.drawable.ic_launcher_foreground,
                    ActSample::class
                ),
                ItemHome(
                    "Toast",
                    R.drawable.ic_launcher_foreground,
                    ActSample::class
                ),
                ItemHome(
                    "Dialog",
                    R.drawable.ic_launcher_foreground,
                    ActSample::class
                )
            )
        )

    }
}

data class ItemHome(val name: String, val image: Int, val clazz: KClass<out Activity>)

class ItemViewHome : ItemViewBuilder<ItemHome, ActHostItemBinding>() {

    override val binding: ActHostItemBinding by viewBind()

    override fun ActHostItemBinding.onBind(position: Int) {
        val item = collection.get(position)
        homeButton.setCompoundDrawablesWithIntrinsicBounds(
            null,
            context.getDrawable(item.image),
            null,
            null
        )
        homeButton.text = item.name
        homeButton.onClick { startActivity(item.clazz) }
    }
}