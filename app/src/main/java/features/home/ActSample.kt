package features.home

import base.ActBind
import base.ActContract
import component.viewBind
import com.google.android.material.bottomsheet.BottomSheetDialog
import extension.onClick
import kotlinfun.app.R
import kotlinfun.app.databinding.ActSampleBinding

class ActSample : ActBind<ActSampleBinding>(), ActContract {
    
    override val binding: ActSampleBinding by viewBind()
    
    override fun ActSampleBinding.onBoundView() {
        defaultFragContainer.onClick {
            val bsd = BottomSheetDialog(this@ActSample)
            bsd.setContentView(R.layout.assets_view)
            bsd.show()
        }
    }
}