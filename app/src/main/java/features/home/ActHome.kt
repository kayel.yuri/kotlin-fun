package features.home

import android.os.Bundle
import android.view.ViewGroup
import base.ActContract
import features.home.ActHomeFragments.FRAG_DETAIL_ID
import features.home.ActHomeFragments.FRAG_HOME_ID
import features.home.ActHomeFragments.FRAG_NOTIFICATIONS_ID
import base.ActBind
import base.loadFrag
import component.viewBind
import features.common.FragDetail
import kotlinfun.app.R
import kotlinfun.app.databinding.ActHomeBinding

object ActHomeFragments {
    const val FRAG_HOME_ID = 0
    const val FRAG_DETAIL_ID = 1
    const val FRAG_NOTIFICATIONS_ID = 2
}

class ActHome : ActBind<ActHomeBinding>(), ActContract {

    override val binding: ActHomeBinding by viewBind()

    override fun ViewGroup.onView() {
        this@ActHome.loadFrag<FragHome>()
    }

    override fun ActHomeBinding.onBoundView() {
        homeNavView.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.nav_home -> dispatchTo(FRAG_HOME_ID)
                R.id.nav_dashboard -> dispatchTo(FRAG_NOTIFICATIONS_ID)
                else -> dispatchTo(FRAG_DETAIL_ID)
            }
            true
        }
    }

    override fun dispatchTo(id: Int, bundle: Bundle?) {
        when (id) {
            FRAG_HOME_ID -> loadFrag<FragHome>(bundle)
            FRAG_DETAIL_ID -> loadFrag<FragDetail>(bundle)
            FRAG_NOTIFICATIONS_ID -> loadFrag<FragViewImplementation>(bundle)
        }
    }

    override fun onBackPressed() {
        if(binding.homeNavView.selectedItemId == R.id.nav_home){
            finish()
        } else {
            binding.homeNavView.selectedItemId = R.id.nav_home
        }
    }
}