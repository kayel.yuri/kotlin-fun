package features.home

import base.ActBind
import extension.bottomNavControl
import component.viewBind
import kotlinfun.app.R
import kotlinfun.app.databinding.ActMainBinding

class ActPager : ActBind<ActMainBinding>() {

    override val binding: ActMainBinding by viewBind()

    override fun ActMainBinding.onBoundView() {
        navView.bottomNavControl(R.id.nav_host_fragment)
    }
}
