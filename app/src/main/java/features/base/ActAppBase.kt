package features.base

import androidx.core.os.bundleOf
import base.*
import component.ItemViewBuilder
import component.setup
import component.viewBind
import extension.listOfAssets
import extension.get
import extension.onClick
import kotlinfun.app.databinding.ActAppBaseBinding
import kotlinfun.app.databinding.ActBaseItemBinding
import model.Asset

private const val FRAG_ACT_BASE = 0
private const val FRAG_ACT_BIND = 1
private const val FRAG_ACT_CONTRACT = 2
private const val FRAG_FRAG_BASE = 3
private const val FRAG_FRAG_BIND = 4
private const val FRAG_ICONTEXT = 5
private const val FRAG_IPERMISSION_RESULT = 6
private const val FRAG_RECYCLER_ADAPTER = 7
private const val FRAG_RECYCLER_HOLDER = 8

class ActAppBase : ActBind<ActAppBaseBinding>(), ActContract {
    
    override val binding: ActAppBaseBinding by viewBind()
    
    override fun ActAppBaseBinding.onBoundView() {
        actAppBaseRecycler.setup<ItemViewAppBase>(listOfAssets("base"))
    }
    
    override fun onItemClick(id: Int) {
        when (id) {
            FRAG_ACT_BASE -> loadFrag<FragActBase>(
                bundleOf(
                    "assets" to arrayListOf(
                        Asset("base", "ActBase.kt"),
                        Asset("res/layout", "act_frame.xml"),
                        Asset("base", "IPermissionResult.kt"),
                        Asset("component", "IContext.kt"),
                        Asset("base", "sample_json.json")
                    )
                )
            )
//            FRAG_ACT_BIND -> loadFrag<FragActBind>(bundle)
//            FRAG_ACT_CONTRACT -> loadFrag<FragActContract>(bundle)
//            FRAG_FRAG_BASE -> loadFrag<FragFragBase>(bundle)
//            FRAG_FRAG_BIND -> loadFrag<FragFragBind>(bundle)
//            FRAG_ICONTEXT -> loadFrag<FragIContext>(bundle)
//            FRAG_IPERMISSION_RESULT -> loadFrag<FragIPermissionResult>(bundle)
//            FRAG_RECYCLER_ADAPTER -> loadFrag<FragRecyclerAdapter>(bundle)
//            FRAG_RECYCLER_HOLDER -> loadFrag<FragRecyclerHolder>(bundle)
        }
    }
    
}

class ItemViewAppBase : ItemViewBuilder<Asset, ActBaseItemBinding>() {
    
    override val binding: ActBaseItemBinding by viewBind()
    
    override fun ActBaseItemBinding.onBind(position: Int) {
        val item = collection.get(position)
        
        baseButton.text = item.name
        baseButton.onClick { (activity as ActContract).onItemClick(position) }
    }
}

