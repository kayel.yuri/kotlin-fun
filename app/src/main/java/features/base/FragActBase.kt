package features.base

import base.FragBind
import component.*
import extension.onClick
import kotlinfun.app.databinding.FragActBaseBinding

class FragActBase : FragBind<FragActBaseBinding>() {
    
    override val binding: FragActBaseBinding by viewBind()
    
    override fun FragActBaseBinding.onBoundView() {
        defaultSingleActionButton.onClick { showAssetSheet() }
    }
}
