package base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import base.FragBase.Companion.new
import extension.*
import kotlinfun.base.R

interface ActContract {

    val appCompatActivity get() = this as AppCompatActivity

    val container: Int get() = R.id.default_frag_container

    fun dispatchTo(id: Int, bundle: Bundle? = null) {}
    
    fun onItemClick(id: Int) {}

    fun onBackPress() = appCompatActivity.onBackPressed()

    fun popBackStack() = appCompatActivity.supportFragmentManager.popBackStack()

}

inline fun <reified Fragment : FragBase> ActContract.loadFrag(
    bundle: Bundle? = null,
    backStack: Boolean = true
) {
    appCompatActivity.supportFragmentManager.run {
        val fragName = Fragment::class.java.name
        if (!popBackStackImmediate(fragName, 0)) {
            ensureTransaction().run {
                if (backStack) {
                    addToBackStack(fragName)
                }
                replace(container, new<Fragment>(bundle), fragName)
                commit()
            }
        }
    }
}