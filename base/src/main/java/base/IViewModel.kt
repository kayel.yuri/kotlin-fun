package base

import androidx.lifecycle.ViewModel

interface IViewModel<Model : ViewModel> {
    val viewModel : Model
}