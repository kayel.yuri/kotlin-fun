package base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import component.IContext
import kotlinfun.base.R

abstract class FragBase(open val layout: Int = R.layout.act_frame) : Fragment(), IContext {
    
    open val contentView: View? = null
    
    companion object {
        inline fun <reified T : FragBase> new(bundle: Bundle? = null): T =
            T::class.java.newInstance().apply { arguments = bundle }
    }
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, state: Bundle?):
            View? = contentView ?: inflater.inflate(layout, container, false)
    
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.onArguments()
        (view as ViewGroup).onView()
    }
    
    open fun Bundle.onArguments() {}
    
    open fun ViewGroup.onView() {}
}