package extension

import android.content.res.Resources

@Suppress("UNCHECKED_CAST") // Converts Pixel value to DensityPixel value
val <N : Number> N.dp get() = (toFloat() * Resources.getSystem().displayMetrics.density) as N

@Suppress("UNCHECKED_CAST") // Converts DensityPixel value to Pixel value
val <N : Number> N.px get() = (toFloat() / Resources.getSystem().displayMetrics.density) as N