package extension

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

fun <Result> launch(block: suspend CoroutineScope.() -> Result) =
    object : CoroutineScope {
        override val coroutineContext: CoroutineContext = IO
        
        var onCompletion: (() -> Result)? = null
        
        init {
            launch { block() }
                .invokeOnCompletion {
                onCompletion?.invoke()
            }
        }
    }


