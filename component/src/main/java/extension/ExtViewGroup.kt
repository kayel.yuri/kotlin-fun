package extension

import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import component.PagerAdapterFrag

fun ViewGroup.bind(layoutID: Int): View = View.inflate(context, layoutID, this)

fun ViewGroup.inflateDetached(layoutID: Int): View =
    LayoutInflater.from(context).inflate(layoutID, null, false)

fun ViewGroup.addViewsAtEnd(
    vararg arrayOfViews: Any?,
    also: (Int, View) -> Unit = { _, _ -> }
) = addViews(arrayOfViews, childCount, also)

fun ViewGroup.addViewsAt(
    atPosition: Int = 0,
    vararg arrayOfViews: Any?,
    also: (Int, View) -> Unit = { _, _ -> }
) = addViews(arrayOfViews, atPosition, also)

fun ViewGroup.addViews(
    vararg arrayOfViews: Any?,
    atPosition: Int = 0,
    also: (Int, View) -> Unit = { _, _ -> }
): ViewGroup {
    val previousChildCount = childCount
    arrayOfViews.forEachIndexed { index, any ->
        if (any is View) {
            addView(any, childCount - previousChildCount + atPosition)
            also.invoke(index, any)
        }
    }
    return this
}

fun ViewPager.setup(frags: Any, titles: Any, tabLayout: TabLayout? = null) {
    tabLayout?.setupWithViewPager(this)
    adapter = PagerAdapterFrag(
        frags, titles, (context as AppCompatActivity).supportFragmentManager
    )
}
