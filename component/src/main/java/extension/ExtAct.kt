@file:Suppress("UNCHECKED_CAST")

package extension

import android.app.Activity
import android.content.Context
import android.os.Build
import androidx.annotation.ColorRes
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.core.content.ContextCompat
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.navigation.Navigation.findNavController
import androidx.navigation.ui.setupWithNavController

fun BottomNavigationView.bottomNavControl(hostID: Int) {
    setupWithNavController(findNavController(context.activity, hostID))
}

fun Activity.setStatusBarColor(@ColorRes colorId: Int, hasLightTextColor: Boolean = true) {
    window.statusBarColor = ContextCompat.getColor(this, colorId)
    if (hasLightTextColor) window.decorView.systemUiVisibility =
        View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
}

fun Activity.hideKeyBoard() =
    (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?)
        ?.hideSoftInputFromWindow(currentFocus?.windowToken, 0)