package extension

import android.content.Context
import android.net.Uri
import androidx.core.content.FileProvider.getUriForFile
import java.io.File

const val APP_AUTH = "kotlinfun.app.provider"
const val MOCKY_AUTH = "kotlinfun.mocky.provider"

fun File.getURI(context: Context): Uri = getURIForFileWithAuth(context)

fun File.getURIForFileWithAuth(context: Context): Uri = try {
    getUriForFile(context, APP_AUTH, this)
} catch (illegalArg: IllegalArgumentException) {
    getUriForFile(context, MOCKY_AUTH, this)
}
