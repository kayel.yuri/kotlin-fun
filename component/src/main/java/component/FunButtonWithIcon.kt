package component

import android.content.Context
import androidx.core.content.res.ResourcesCompat.getFont
import androidx.appcompat.widget.AppCompatTextView
import android.util.AttributeSet
import android.widget.Button
import extension.viewAs
import kotlinfun.component.R

class FunButtonWithIcon @JvmOverloads constructor(context: Context, attributeSet: AttributeSet? = null, defStyleAttr: Int = 0)
    : BaseFunButton(context, attributeSet, defStyleAttr) {

    init {
        inflate(context, R.layout.view_fun_button_with_icon, this)
        buttonTitle = findViewById<AppCompatTextView>(R.id.fun_button_title)
        buttonTitle.typeface = getFont(context, R.font.roboto_bold)
        attributeSet?.let { setAttributes(it) }
        this viewAs Button::class
    }

    override fun setAttributes(attributeSet: AttributeSet) {
        context.obtainStyledAttributes(attributeSet,
            R.styleable.FunButtonWithIcon, 0,
            R.style.FunButtonWithIcon
        ).run {
            buttonTitle.run {
                text = getString(R.styleable.FunButtonWithIcon_text)
                typeface = getFont(context, getResourceId(
                    R.styleable.FunButtonWithIcon_textFont,
                    R.font.roboto_bold
                ))
            }
            findViewById<AppCompatTextView>(R.id.fun_button_subtitle).run {
                text = getString(R.styleable.FunButtonWithIcon_subText)
                typeface = getFont(context, getResourceId(
                    R.styleable.FunButtonWithIcon_subTextFont,
                    R.font.roboto_regular
                ))
            }
            findViewById<AppCompatTextView>(R.id.fun_button_arrow).text = getString(R.styleable.FunButtonWithIcon_fontIcon)
            isEnabled = getBoolean(R.styleable.FunButtonWithIcon_enabled, true)
            background = getDrawable(R.styleable.FunButtonWithIcon_backgroundRipple)
            recycle()
        }
    }
}
