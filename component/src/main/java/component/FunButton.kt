package component

import android.content.Context
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat.*
import androidx.appcompat.widget.AppCompatTextView
import android.util.AttributeSet
import android.widget.Button
import extension.viewAs
import kotlinfun.component.R

class FunButton @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyleAttr: Int = 0
) :
    BaseFunButton(context, attributeSet, defStyleAttr) {

    var outlined = false

    init {
        inflate(context, R.layout.view_fun_button_single_line, this)
        buttonTitle = findViewById<AppCompatTextView>(R.id.fun_button_title)
        buttonTitle.typeface = getFont(context, R.font.roboto_bold)
        attributeSet?.let { setAttributes(it) }
        this viewAs Button::class
    }

    override fun setAttributes(attributeSet: AttributeSet) {
        context.obtainStyledAttributes(
            attributeSet,
            R.styleable.FunButton, 0,
            R.style.FunButton
        ).run {
            buttonTitle.run {
                text = getString(R.styleable.FunButton_text)
                setTextColor(
                    getColor(
                        R.styleable.FunButton_textColor, ContextCompat.getColor(
                            context,
                            R.color.white
                        )
                    )
                )
                typeface = getFont(
                    context, getResourceId(
                        R.styleable.FunButton_textFont,
                        R.font.roboto_bold
                    )
                )
            }
            isEnabled = getBoolean(R.styleable.FunButton_enabled, true)
            outlined = getBoolean(R.styleable.FunButton_outlined, false)
            background = if (outlined) {
                buttonTitle.setTextColor(getColor(resources, R.color.purple_500, null))
                getDrawable(context.resources, R.drawable.fun_button_outlined, null)
            } else {
                getDrawable(R.styleable.FunButton_backgroundRipple)
            }
            recycle()
        }
    }
}
