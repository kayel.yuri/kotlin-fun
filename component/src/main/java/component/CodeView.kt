package component

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color.TRANSPARENT
import android.util.AttributeSet
import android.view.Gravity.CENTER
import android.webkit.WebView
import extension.readAsset

private const val TYPE_TEXT = "text"
private const val TYPE_CODE = "code"
private const val TYPE_HTML = "html"

private const val HTML_HEAD = "<html><head>"
private const val HTML_HEAD_END = "</head><body>"
private const val HTML_BODY_END = "</body></html>"
private const val STYLE_TEXT_WRAP = "\"<style>\" +\n" +
        "                \"pre {\\n\" +\n" +
        "                \"    white-space: pre-wrap;       /* CSS 3 */\n" +
        "                \"    white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */\n" +
        "                \"    white-space: -pre-wrap;      /* Opera 4-6 */\n" +
        "                \"    white-space: -o-pre-wrap;    /* Opera 7 */\n" +
        "                \"    word-wrap: break-word;       /* Internet Explorer 5.5+ */\n" +
        "                \"} \n" +
        "                \"</style>\n"
private const val HTML_PRE_TEXT_START = "<pre><code class=\"nohighlight\">"
private const val HTML_PRE_TEXT_END = "</code></pre> \n"
private const val HTML_PRE_CODE_START = "<pre style=\"white-space: no-wrap;\"><code class=\""
private const val HTML_PRE_CODE_WRAP_START = "<pre style=\"white-space: pre-wrap;\"><code class=\""
private const val HTML_PRE_CODE_CLASS = "\">"
private const val HTML_PRE_CODE_END = "</code></pre> \n"

@SuppressLint("SetJavaScriptEnabled")
class CodeView @JvmOverloads constructor(
    context: Context,
    set: AttributeSet? = null,
    attr: Int = 0
) : WebView(context, set, attr) {

    init {
        setBackgroundColor(TRANSPARENT)
        settings.javaScriptEnabled = true
        foregroundGravity = CENTER
    }

    val DEFAULT_STYLE get() = ANDROIDSTUDIO
    val ANDROIDSTUDIO get() = context.readAsset("androidstudio".style)
    val HLJS get() = context.readAsset("default".style)
    val AGATE get() = context.readAsset("agate".style)
    val ARDUINO_LIGHT get() = context.readAsset("arduino-light".style)
    val ARTA get() = context.readAsset("arta".style)
    val ASCETIC get() = context.readAsset("ascetic".style)
    val ATELIER_DARK get() = context.readAsset("atelier-cave-dark".style)
    val ATELIER_LIGHT get() = context.readAsset("atelier-cave-light".style)
    val ATELIER_FOREST_DARK get() = context.readAsset("atelier-forest-dark".style)
    val DARKULA get() = context.readAsset("darcula".style)
    val DARKSTYLE get() = context.readAsset("dark".style)
    val DOCCO get() = context.readAsset("docco".style)
    val FAR get() = context.readAsset("far".style)
    val GITHUB get() = context.readAsset("github".style)
    val GITHUBGIST get() = context.readAsset("github-gist".style)
    val GOOGLECODE get() = context.readAsset("googlecode".style)
    val IDEA get() = context.readAsset("idea".style)
    val MAGULA get() = context.readAsset("magula".style)
    val OBSIDIAN get() = context.readAsset("obsidian".style)
    val XCODE get() = context.readAsset("xcode".style)

    private val String.style get() = "highlight/styles/$this.css"
    private val wrapper: MutableList<String> = mutableListOf("", "", "", "", "")
    private val dataList: MutableList<Data> = mutableListOf()
    private val hljs = "<script>${context.readAsset("highlight/highlight.pack.js")}</script>\n"
    private val lineNum = "<script>${context.readAsset("highlight/hljs-line-num.js")}</script>\n"

    var wrap = false

    var style: String = ANDROIDSTUDIO
        set(value) {
            field = if (value.isNotEmpty()) value else DEFAULT_STYLE
        }

    var language: String = Language.KOTLIN
        set(value) {
            field = if (value.isNotEmpty()) value else Language.DEFAULT
        }

    var code: String = ""
        set(value) {
            field = value
            dataList.add(Data(value.escapeHTML(), TYPE_CODE))
        }

    var text: String = ""
        set(value) {
            field = value
            dataList.add(Data(value.escapeHTML(), TYPE_TEXT))
        }

    var html: String = ""
        set(value) {
            field = value
            dataList.add(Data(value, TYPE_HTML))
        }

    fun setup(block: CodeView.() -> Unit = {}): CodeView {
        resetData()
        resetWrapper()
        block()
        load()
        return this
    }

    private fun String.escapeHTML() = replace("<", "&lt;")
        .replace(">", "&gt;")

    fun addHTMLHead(content: String) = apply { wrapper[1] += "$content \n" }

    @SuppressLint("SetJavaScriptEnabled")
    fun load() = loadDataWithBaseURL(
        null,
        build(wrapper, Settings(wrap, style, language, dataList)),
        "text/html",
        "utf-8",
        ""
    )

    fun build(list: MutableList<String>, settings: Settings): String {
        list[0] = HTML_HEAD.trimIndent()
        list[1] = "${list[1]}<style>${settings.style}</style>".trimIndent()
        list[1] = list[1] + hljs + lineNum
        if (settings.wrap) {
            list[1] = "${list[1]}<style>$STYLE_TEXT_WRAP</style> \n"
        }
        list[1] = "${list[1]}<script>hljs.initHighlightingOnLoad();</script> \n" +
                "<script>hljs.initLineNumbersOnLoad();</script> \n"
        list[2] = HTML_HEAD_END.trimIndent()
        list[3] = list[3] + readData(settings.data, settings.wrap, settings.lang)
        list[4] = HTML_BODY_END
        val output = list[0] + list[1] + list[2] + list[3] + list[4]
        return output
    }

    private fun readData(list: List<Data>, wrap: Boolean, lang: String) =
        if (list.isEmpty()) list.toString() else {
            var text = ""
            list.forEach {
                text += when (it.type) {
                    TYPE_CODE -> {
                        (if (wrap) HTML_PRE_CODE_WRAP_START else HTML_PRE_CODE_START) +
                                lang + HTML_PRE_CODE_CLASS + it.text + HTML_PRE_CODE_END
                    }
                    TYPE_HTML -> it.text
                    else -> HTML_PRE_TEXT_START + it.text + HTML_PRE_TEXT_END
                }
            }
            text
        }

    private fun resetData() {
        dataList.clear()
    }

    private fun resetWrapper() {
        wrapper.clear()
        wrapper.addAll(listOf("", "", "", "", ""))
    }
}

class Data(val text: String, val type: String)

class Settings(
    val wrap: Boolean,
    val style: String,
    val lang: String,
    val data: List<Data>
)

object Language {
    const val DEFAULT = "kotlin"
    const val KOTLIN = "kotlin"
    const val JAVA = "java"
    const val JAVASCRIPT = "javascript"
    
    const val XML = "xml"
    const val HTML = "html"
    const val SQL = "sql"
    
    const val C = "c"
    const val CPLUS = "c++"
    const val CSHARP = "c#"
    
    const val PYTHON = "python"
    const val RUBY = "ruby"
    const val PHP = "php"
}

object Extension {
    const val KT = "kt"
    const val JSON = "json"
}

