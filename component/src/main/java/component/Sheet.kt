package component

import android.content.Context
import android.content.DialogInterface
import android.view.View
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinfun.component.R

class Sheet(
    context: Context,
    layout: Any = R.layout.assets_view,
    cancelable: Boolean = true,
    cancelListener: DialogInterface.OnCancelListener? = null,
    init: Sheet.() -> Unit = {}
) : BottomSheetDialog(context, cancelable, cancelListener) {
    
    init {
        if (layout is Int) setContentView(layout) else setContentView(layout as View)
        init()
    }
    
}