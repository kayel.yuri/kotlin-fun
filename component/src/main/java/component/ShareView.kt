package component

import android.widget.TextView
import android.content.Context
import android.content.Intent.ACTION_SEND
import android.content.Intent.ACTION_VIEW
import android.graphics.Bitmap.CompressFormat.JPEG
import android.graphics.Color.WHITE
import androidx.appcompat.app.AppCompatActivity
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.FrameLayout
import android.widget.Toast.LENGTH_LONG
import android.widget.Toast.makeText
import extension.*
import kotlinfun.component.R
import java.io.FileOutputStream

const val HIGH_QUALITY_PHOTO = 80
const val SHARE = ACTION_SEND
const val SAVE = ACTION_VIEW
private val EMPTY_BLOCK: (() -> Unit) -> Unit = {}

fun PrintableView.newShareView(printableView: PrintableView? = this@newShareView, layout: Int = R.layout.share_view) =
    ShareView(viewContext).apply {
        this.printableView = printableView
        this.layout = layout
    }

class ShareView @JvmOverloads constructor(context: Context, val set: AttributeSet? = null, defStyle: Int = 0) :
    FrameLayout(context, set, defStyle), ViewPrinter, IPermissionRequester {

    override var activity: AppCompatActivity = context as AppCompatActivity

    var layout = R.layout.share_view
    var printableView: PrintableView? = null
        set(value) {
            field = value
            field?.printer = this
        }
    var printableViewID = 0
    var printBGColor = WHITE
    var shareViewText: String? = null
    var openViewText: String? = null
    var toast = false
    var mayCallSaveIntent = true
    var mayCallShareIntent = true
    var toastText: String? = null
    var action = SAVE

    private val buttonSave: TextView by lazy { findViewById<TextView>(R.id.share_view_button_save) }

    private val buttonShare: TextView by lazy { findViewById<TextView>(R.id.share_view_button_share) }

    var onSave: ((() -> Unit) -> Unit) = EMPTY_BLOCK
        set(onClick) {
            field = onClick
            buttonSave.setOnClickAndCallBack(onClick) { save() }
            if (onShare == EMPTY_BLOCK) onShare = onSave
        }

    var onShare: ((() -> Unit) -> Unit) = EMPTY_BLOCK
        set(onClick) {
            field = onClick
            buttonShare.setOnClickAndCallBack(onClick) { share() }
            if (onSave == EMPTY_BLOCK) onSave = onShare
        }

    private val chooserText
        get() = when (action) {
            SHARE -> shareViewText
            else -> {
                if (toast) makeText(viewContext, toastText, LENGTH_LONG).show()
                openViewText
            }
        }

    init {
        linearParams(MATCH_PARENT)
        bind(layout)
        obtainStyledAttributes(set, R.styleable.ShareView, R.style.ShareView) {
            printableViewID = getResourceId(R.styleable.ShareView_printableView, 0)
            printBGColor = getColor(R.styleable.ShareView_printBackgroundColor, WHITE)
            shareViewText = getString(R.styleable.ShareView_shareViewText)
            openViewText = getString(R.styleable.ShareView_openViewText)
            toast = getBoolean(R.styleable.ShareView_toast, false)
            toastText = getString(R.styleable.ShareView_toastText)
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        if (printableView == null && printableViewID != 0) {
            printableView = (parent as View).findViewById<View>(printableViewID) as PrintableView
            printableView?.printer = this
        }
        requestLayout()
    }

    fun save() {
        action = SAVE
        requestPermission()
    }

    fun share() {
        action = SHARE
        requestPermission()
    }

    override fun onPermissionGranted() {
        printableView?.setupAndPrintView()
    }

    override fun printView(view: View) {
        buildFile(printableView?.printableFileName!!)?.run {
            FileOutputStream(this).let {
                view.createBitmap(printBGColor)?.compress(JPEG, HIGH_QUALITY_PHOTO, it)
                if (mayCallSaveIntent && action == SAVE || mayCallShareIntent && action == SHARE) {
                    startImageIntent(action, getURI(context), chooserText)
                }
                it.delayedClose()
            }
        }
    }

    private fun FileOutputStream.delayedClose() {
        flush()
        postDelayed({ close() }, DEFAULT_WAIT_FOR_ANIM)
    }
}

interface ViewPrinter : IView {
    fun printView(view: View)
    fun delayedPrintView(view: View) = view.waitForDefaultAnim { printView(view) }
}

interface PrintableView : IView {
    var printer: ViewPrinter?
    var printableFileName: String
    fun setupAndPrintView() {
        printer?.printView(asView)
    }
}
