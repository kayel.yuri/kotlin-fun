package component

import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import android.view.ViewGroup

abstract class RecyclerAdapter<Type : ViewHolder>(var collection: Collection<*>) :
    Adapter<Type>() {

    abstract var onTarget: () -> Unit

    abstract fun getTarget(): Int
}

inline fun <reified T : ItemViewBuilder<*, *>> RecyclerView.setup(list: Collection<*>) =
    recyclerAdapter<T>(list).apply {
        adapter = this
    }

inline fun <reified Builder : ItemViewBuilder<*, *>> recyclerAdapter(collection: Collection<*>) =
    object : RecyclerAdapter<RecyclerViewHolder>(collection) {

        override var onTarget: () -> Unit = {}

        override fun getTarget() = collection.size - 10

        override fun onCreateViewHolder(recycler: ViewGroup, viewType: Int) =
            Builder::class.java.newInstance().run {
                init(recycler, collection)
                RecyclerViewHolder(this)
            }

        override fun getItemCount() = collection.size

        override fun onBindViewHolder(viewHolder: RecyclerViewHolder, position: Int) {
            if (position == getTarget()) {
                onTarget()
            }
            viewHolder.builder.onBind(position)
        }
    }