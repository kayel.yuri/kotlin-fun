package component

import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.content.pm.PackageManager.PERMISSION_GRANTED
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.appcompat.app.AppCompatActivity

private const val PERMISSION_REQUEST_CODE = 11

interface IRequestPermission {
    var iPermissionRequester: IPermissionRequester?

    fun requestPermissionResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == PERMISSION_REQUEST_CODE && grantResults.isNotEmpty()) {
            iPermissionRequester?.run { if (grantResults[0] == PERMISSION_GRANTED) onPermissionGranted() else onPermissionDenied() }
        }
    }
}

interface IPermissionRequester {
    var activity: AppCompatActivity

    fun requestPermission(permission: String = WRITE_EXTERNAL_STORAGE) =
        if (checkSelfPermission(activity, permission) == PERMISSION_GRANTED) onPermissionGranted() else onRequest(permission)

    private fun onRequest(permission: String) {
        (activity as IRequestPermission).iPermissionRequester = this
        requestPermissions(activity, arrayOf(permission),
            PERMISSION_REQUEST_CODE
        )
        onPermissionRequested()
    }

    fun onPermissionGranted() {}

    fun onPermissionRequested() {}

    fun onPermissionDenied() {}
}
