package component

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity.*
import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast.LENGTH_SHORT
import android.widget.Toast.makeText
import extension.EMPTY_STRING
import extension.*
import kotlinfun.component.R

fun View.newMenuButton(leftIcon: Any, text: Any, rightIcon: Any = EMPTY_STRING) = MenuButton(context).apply {
    this.leftIcon.text = stringAny(leftIcon)
    this.title.text = stringAny(text)
    this.rightIcon.text = stringAny(rightIcon)
    this setID text
}

class MenuButton @JvmOverloads constructor(context: Context, set: AttributeSet? = null, defStyleAttr: Int = 0) :
    LinearLayout(context, set, defStyleAttr) {

    private val iconWidthDP = dpToPx(DEFAULT_ICON_WIDTH)

    val leftIcon: TextView by lazy {
        newText(R.style.funIconPurple24, padding = 8).apply {
            linearParams(iconWidthDP, MATCH_PARENT).gravity = START or CENTER_VERTICAL
            gravity = CENTER
            enforceFocus(false)
        }
    }

    val title: TextView by lazy {
        newText(R.style.funTextBlack16, padding = 0).apply {
            linearParams(0, MATCH_PARENT).weight = 1f
            setPaddingDP(DEFAULT_PADDING, 0, 0, 0)
            gravity = START or CENTER_VERTICAL
            enforceFocus(false)
        }
    }

    val rightIcon: TextView by lazy {
        newText(R.style.funIconPurple24, padding = 8).apply {
            linearParams(iconWidthDP, MATCH_PARENT).gravity = END or CENTER_VERTICAL
            gravity = CENTER
            enforceFocus(false)
        }
    }

    var leftIconColor: Int = R.color.black
        set(value) {
            field = value
            leftIcon.setTextColor(value)
        }

    var rightIconColor: Int = R.color.black
        set(value) {
            field = value
            rightIcon.setTextColor(value)
        }

    var textColor: Int = R.color.black
        set(value) {
            field = value
            title.setTextColor(value)
        }

    init {
        this viewAs Button::class
        title.gravity = START or CENTER_VERTICAL
        orientation = HORIZONTAL
        gravity = CENTER
        obtainStyledAttributes(set, R.styleable.MenuButton) {
            leftIcon.text = getString(R.styleable.MenuButton_left_icon) ?: context.getString(R.string.ic_left_arrow)
            rightIcon.text = getString(R.styleable.MenuButton_right_icon) ?: EMPTY_STRING
            title.text = getString(R.styleable.MenuButton_text) ?: EMPTY_STRING
            leftIconColor = getColor(R.styleable.MenuButton_left_icon_color, getColor(R.color.orange_500))
            rightIconColor = getColor(R.styleable.MenuButton_left_icon_color, getColor(R.color.orange_500))
            textColor = getColor(R.styleable.MenuButton_text_color, getColor(R.color.black))
        }
        background = getDrawable(R.drawable.cornered_white)
        enforceFocus(true)
        addViews(leftIcon, title, rightIcon)
        addBackgroundRipple()
        setOnClickListener { makeText(context, title.text, LENGTH_SHORT).show() }
    }
}