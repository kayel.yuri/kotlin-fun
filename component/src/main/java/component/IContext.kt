package component

import android.app.Activity
import android.app.Dialog
import android.view.LayoutInflater
import android.view.View
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import extension.activity
import extension.inflater

interface IContext {

    val activity
        get() : Activity = when (this) {
            is Fragment -> requireContext().activity
            is View -> context.activity
            is ItemViewBuilder<*, *> -> context.activity
            is Dialog -> context.activity
            else -> this as Activity
        }
}

@Suppress("UNCHECKED_CAST")
inline fun <reified Binding : ViewBinding> IContext.viewBind() = lazy {
    Binding::class.java.getMethod("inflate", LayoutInflater::class.java)
        .invoke(null, activity.inflater) as Binding
}