package component

import android.content.Context
import android.util.AttributeSet
import android.view.KeyEvent.ACTION_DOWN
import android.view.View
import android.view.animation.AnimationUtils.loadAnimation
import kotlinfun.component.R

class SingleActionButton @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {
    
    init {
        setBackgroundResource(R.drawable.ic_gear)
        val anim = loadAnimation(context, R.anim.rotate)
        setOnTouchListener { _, event ->
            if (event.action == ACTION_DOWN) {
                startAnimation(anim)
                postDelayed({ performClick() }, 50)
            }
            true
        }
    }
}