package component

import android.content.Context
import android.os.Parcelable
import android.util.AttributeSet
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import extension.*
import kotlinfun.component.R

open class LinearBuilder @JvmOverloads constructor(
    context: Context,
    set: AttributeSet? = null,
    attr: Int = 0
) :
    PrintLinearLayout(context, set, attr) {

    var buildIntent: Int = 0

    var response: Parcelable? = null
        set(response) {
            response?.let {
                field = it
                it.responseBuilder()
            }
        }

    fun build(any: Parcelable?, id: Int): LinearBuilder {
        buildIntent = id
        response = any
        return this
    }

    private fun <T> T.responseBuilder() = when (this) {
        is Parcelable -> null
        else -> null
    }
}

abstract class BaseViewBuilder<T>(builderView: LinearBuilder) {
    @Suppress("UNCHECKED_CAST")
    val response: T = builderView.response as T

    init {
        response.run {
            build()
        }
    }

    abstract fun T.build(): Any?
}

fun LinearBuilder.buildSample() = object
    : BaseViewBuilder<Parcelable>(this) {

    override fun Parcelable.build() = when (buildIntent) {
        0 -> buildCaseEffectivation()
        else -> buildCaseEffectivation()
    }

    private fun Parcelable.buildCaseEffectivation() = addViews(
        newToolbar("<-", "titulo"),
        newCollumn(
            pagamentoBoleto,
            valor,
            identidade,
            dataPayment,
            newShareView(this@buildSample)
        ).also { linear ->
            linear.linearMargins(orient = MARGIN_ALL, width = MATCH_PARENT)
            linear.setBackgroundResource(R.drawable.cornered_white)
        }
    )

    private val Parcelable.pagamentoBoleto
        get() = newText(text ="pagamento boleto").apply { setHeaderAccessibility() }
    private val Parcelable.valor
        get() = newText(text="any")
    private val Parcelable.identidade
        get() = newLabel(label="any", value = "any")
    private val Parcelable.dataPayment
        get() = newLabel(label="any", value = "any")
}