package component

import android.annotation.SuppressLint
import android.content.Context
import androidx.appcompat.widget.AppCompatCheckBox
import android.util.AttributeSet
import android.widget.Toast.LENGTH_LONG
import android.widget.Toast.makeText
import extension.addBackgroundRipple
import extension.getFont
import extension.setPaddingDP
import kotlinfun.component.R

@SuppressLint("RestrictedApi", "Recycle")
class FunCheckBox : AppCompatCheckBox {

    private var showToast = true
    private var inverted = false

    constructor(context: Context?) : super(context) {
        init(null)
    }

    constructor(context: Context?, attributeSet: AttributeSet?) : super(context, attributeSet) {
        init(attributeSet)
    }

    constructor(context: Context?, attributeSet: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attributeSet,
        defStyleAttr
    ) {
        init(attributeSet)
    }

    fun init(attributeSet: AttributeSet?) {
        attributeSet?.let { set ->
            context.obtainStyledAttributes(
                set,
                R.styleable.FunCheckBox, 0,
                R.style.FunCheckBox
            ).also {
                supportButtonTintList = it.getColorStateList(R.styleable.FunCheckBox_color)
                showToast = it.getBoolean(R.styleable.FunCheckBox_showToast, true)
                inverted = it.getBoolean(R.styleable.FunCheckBox_inverted, false)
                it.recycle()
            }
        }

        if (inverted) {
            layoutDirection = LAYOUT_DIRECTION_RTL
        }
        addBackgroundRipple(true)
        setPaddingDP()
        typeface = getFont(R.font.roboto_bold)

        if (isEnabled) {
            setOnCheckedChangeListener { _, isChecked -> setChecked(isChecked, showToast) }
            contentDescription = text
        }
    }

    fun setChecked(isChecked: Boolean = true, showToast: Boolean = false) {
        setChecked(isChecked)
        if (showToast) makeText(context, text, LENGTH_LONG).show()
    }
}
