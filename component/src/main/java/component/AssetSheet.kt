package component

import android.view.View
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.google.android.material.bottomsheet.BottomSheetDialog
import component.Extension.JSON
import component.Extension.KT
import component.Language.JAVA
import component.Language.JAVASCRIPT
import component.Language.KOTLIN
import component.Language.XML
import extension.get
import extension.onClick
import extension.readAsset
import kotlinfun.component.R
import kotlinfun.component.databinding.ItemAssetBinding
import kotlinfun.component.databinding.ViewCodeviewBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import model.Asset

fun Fragment.showAssetSheet() {
    BottomSheetDialog(requireContext()).apply {
        setContentView(R.layout.assets_view)
        findViewById<VerticalRecycler>(R.id.assets_view_recycler)
            ?.setup<ItemAssetBuilder>(arguments?.getParcelableArrayList<Asset>("assets")!!)
        show()
    }
}

class ItemAssetBuilder : ItemViewBuilder<Asset, ItemAssetBinding>() {
    
    override val binding: ItemAssetBinding by viewBind()
    
    override fun ItemAssetBinding.onBind(position: Int) {
        val asset = collection.get(position)
        var codeString = ""
        
        CoroutineScope(IO).launch {
            codeString = context.readAsset(asset.fullpath)
        }
        
        
        itemAssetItem.run {
            text = asset.name
            
            setCompoundDrawablesRelativeWithIntrinsicBounds(
                when (asset.extension) {
                    KT   -> R.drawable.ic_kotlin
                    XML  -> R.drawable.ic_xml
                    JSON -> R.drawable.ic_json
                    else -> R.drawable.ic_launcher_foreground
                }, 0, 0, 0
            )
            
            onClick {
                postDelayed({
                    newPanel<ViewCodeviewBinding> {
                        viewCodeviewFrame.onClick(it::dismiss)
                        defaultCodeview.setup {
                            language = when (asset.extension) {
                                KT   -> KOTLIN
                                XML  -> XML
                                JSON -> JAVASCRIPT
                                else -> JAVA
                            }
                            code = codeString
                        }
                    }
                }, 70)
            }
        }
    }
}
