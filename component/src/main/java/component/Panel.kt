@file:Suppress("UNUSED_PARAMETER")

package component

import android.content.Context
import android.content.DialogInterface.OnShowListener
import android.graphics.Color.TRANSPARENT
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatDialog
import android.view.View
import android.view.Window.FEATURE_NO_TITLE
import androidx.viewbinding.ViewBinding
import extension.DEFAULT_WAIT_FOR_ANIM
import extension.launch
import extension.waitForDefaultAnim
import kotlinfun.component.R
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO

inline fun <reified Binding : ViewBinding> View.newPanel(
    layout: Any = R.layout.panel,
    onShow: OnShowListener? = null,
    noinline init: (Binding.(Panel<Binding>) -> Unit) = {}
) = object : Panel<Binding>(context, onShow, init, layout) {
    
    val binding by viewBind<Binding>()
    
    init {
        setContentView(binding.root)
        setOnShowListener(onShow)
        init(binding, this)
        show()
    }
}

open class Panel<Binding : ViewBinding>(
    context: Context,
    onShow: OnShowListener? = null,
    init: (Binding.(Panel<Binding>) -> Unit),
    layout: Any
) : AppCompatDialog(context, R.style.Panel), IContext
