package component

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import androidx.annotation.LayoutRes
import androidx.annotation.VisibleForTesting
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import extension.*
import kotlinfun.component.R

const val API29_RESOURCES_POLLY_FILL_ID_NULL = 0

class ReceiptView : FrameLayout, PrintableView {

    override var printer: ViewPrinter? = null
    override lateinit var printableFileName: String
    var balanceListenner: BalanceInfoListenner? = null
    private var balance: String? = null
    lateinit var boxAllInfo: ViewGroup

    private lateinit var messageView: TextView

    @VisibleForTesting
    lateinit var titleView: TextView

    @VisibleForTesting
    lateinit var infoView: LinearLayout

    @VisibleForTesting
    lateinit var infoDetailsView: LinearLayout

    @VisibleForTesting
    lateinit var balanceView: TextView

    @VisibleForTesting
    lateinit var balanceButton: TextView

    @VisibleForTesting
    lateinit var infoDetailsButton: TextView

    @VisibleForTesting
    lateinit var loadingBalance: View

    constructor(context: Context) : super(context) {
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(
        context,
        attrs,
        defStyleAttr,
        defStyleRes
    ) {
        init(attrs)
    }

    @VisibleForTesting
    fun init(attrs: AttributeSet?) {
        inflateLayout()
        boxAllInfo = findViewById(R.id.box_all_info)
        infoView = findViewById(R.id.box_info)
        titleView = findViewById(R.id.title_label)
        messageView = findViewById(R.id.message_label)
        configDetails()
        configBalance()
        attrs?.let { attrSet ->
            context.obtainStyledAttributes(attrSet, R.styleable.ReceiptView, 0, 0).run {
                setInfoView(
                    getResourceId(
                        R.styleable.ReceiptView_infoLayout,
                        API29_RESOURCES_POLLY_FILL_ID_NULL
                    )
                )
                setInfoDetails(
                    getResourceId(
                        R.styleable.ReceiptView_infoDetailsLayout,
                        API29_RESOURCES_POLLY_FILL_ID_NULL
                    )
                )
                showDetails(getBoolean(R.styleable.ReceiptView_showDetails, false))
                setTitle(getString(R.styleable.ReceiptView_title))
                setMessage(getString(R.styleable.ReceiptView_message))
                setBalance(getString(R.styleable.ReceiptView_balance))
                showBalance(getBoolean(R.styleable.ReceiptView_showBalance, false))
                recycle()
            }
        }
        enforceFocus(false)
    }

    @VisibleForTesting
    fun inflateLayout() {
        inflate(context, R.layout.view_comprovantes, this)
    }

    fun getMessage() = messageView.text

    fun setMessage(message: String?) {
        messageView.text = message
    }

    fun getTitle() = titleView.text

    fun setTitle(title: String?) {
        titleView.text = title
        printableFileName = title ?: ""
    }

    @VisibleForTesting
    fun configBalance() {
        balanceView = findViewById(R.id.text_balance)
        balanceButton = findViewById(R.id.btn_balance)
        loadingBalance = findViewById(R.id.loading_balance)
        balanceButton.setOnClickListener { onBalance() }
        balanceView.setOnClickListener { onBalance() }
    }

    private fun onBalance() {
        if (balance == null) {
            balanceListenner?.onRequestBalance()
            showLoading()
        } else {
            showBalance(balanceView.visibility != VISIBLE)
        }
    }

    fun getBalance() = balance

    fun setBalance(balance: String?) {
        this.balance = balance
        balanceView.text = balance
        if (loadingBalance.visibility == VISIBLE) {
            hideLoading()
        }
    }

    fun setBalanceError() {
        balance = null
        hideLoading()
        balanceButton.setText(R.string.proof_btn_details_try_again)
    }

    @VisibleForTesting
    fun showLoading() {
        loadingBalance.animateVisibility()
        balanceButton.animateVisibility(0f, INVISIBLE)
    }

    @VisibleForTesting
    fun hideLoading() {
        loadingBalance.animateVisibility(0f, GONE)
        balanceButton.animateVisibility()
        showBalance(true)
    }

    @VisibleForTesting
    fun showBalance(show: Boolean) {
        if (show && balance != null) {
            balanceView.run {
                animateVisibility()
                delayedFocus()
            }
            balanceButton.setText(R.string.proof_btn_balance_hide)
        } else {
            balanceView.animateVisibility(0f, INVISIBLE)
            balanceButton.run {
                setText(R.string.proof_btn_balance_show)
                delayedFocus()
            }
        }
    }

    @VisibleForTesting
    fun configDetails() {
        infoDetailsView = findViewById(R.id.box_info_details)
        infoDetailsButton = findViewById(R.id.btn_info_details)
        infoDetailsButton.setOnClickListener {
            showDetails(infoDetailsView.isHeightDiffMeasuredHeight())
        }
    }

    @VisibleForTesting
    fun showDetails(show: Boolean) {
        infoDetailsView.animateExpand(show)
        infoDetailsButton.setText(if (show) R.string.proof_btn_details_close else R.string.proof_btn_details_open)
        getInfoDetailsView()?.delayedFocus()
    }

    fun isDetailsExpanded(): Boolean = infoDetailsView.height != 0

    fun getInfoView(): View? = infoView.getChildAt(0)

    @VisibleForTesting
    fun setInfoView(@LayoutRes layoutId: Int) {
        if (layoutId == API29_RESOURCES_POLLY_FILL_ID_NULL) {
            return
        }
        setInfoView(inflate(context, layoutId, null))
    }

    @VisibleForTesting
    fun setInfoView(view: View?) {
        infoView.removeAllViews()
        view?.let {
            infoView.addView(it, 0)
        }
    }

    fun getInfoDetailsView(): View? = infoDetailsView.getChildAt(0)

    fun setInfoDetails(@LayoutRes layoutId: Int) {
        if (layoutId == API29_RESOURCES_POLLY_FILL_ID_NULL) {
            return
        }
        setInfoDetails(inflate(context, layoutId, null))
    }

    fun setInfoDetails(view: View?) {
        infoDetailsView.removeAllViews()
        if (view == null) {
            showDetails(false)
            if (infoDetailsView.childCount == 0) {
                infoDetailsButton.visibility = GONE
            }
        } else {
            infoDetailsView.addView(view, 0)
        }
    }

    override fun setupAndPrintView() {
        showDetails(true)
        printer?.delayedPrintView(boxAllInfo)
    }

    interface BalanceInfoListenner {
        fun onRequestBalance()
    }
}
