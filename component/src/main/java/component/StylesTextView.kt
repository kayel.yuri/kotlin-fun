package component

import android.content.Context
import androidx.appcompat.widget.AppCompatTextView
import android.text.SpannableStringBuilder
import android.util.AttributeSet
import android.widget.TextView.BufferType.SPANNABLE
import extension.EMPTY_STRING
import extension.buildSpannable
import extension.enforceFocus
import extension.obtainStyledAttributes
import kotlinfun.component.R

class StylesTextView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AppCompatTextView(context, attrs, defStyleAttr) {

    private var mayUpdate: Boolean = false
        set(value) {
            field = value
            update()
        }

    var text0: CharSequence = EMPTY_STRING
        set(value) {
            field = value
            update()
        }

    var text1: CharSequence = EMPTY_STRING
        set(value) {
            field = value
            update()
        }

    var text2: CharSequence = EMPTY_STRING
        set(value) {
            field = value
            update()
        }

    var style0: Int = R.style.funTextGrayDark16
        set(value) {
            field = value
            update()
        }

    var style1: Int = R.style.funTextGrayDark16Bold
        set(value) {
            field = value
            update()
        }

    var style2 = style0
        set(value) {
            field = value
            update()
        }

    init {
        obtainStyledAttributes(attrs, R.styleable.StylesTextView, R.style.DefaultTextView) {
            text0 = getString(R.styleable.StylesTextView_text0) ?: EMPTY_STRING
            text1 = getString(R.styleable.StylesTextView_text1) ?: EMPTY_STRING
            text2 = getString(R.styleable.StylesTextView_text2) ?: EMPTY_STRING
            style0 = getResourceId(
                R.styleable.StylesTextView_style0,
                R.style.funTextGrayDark16
            )
            style1 = getResourceId(
                R.styleable.StylesTextView_style1,
                R.style.funTextGrayDark16Bold
            )
            style2 = getResourceId(
                R.styleable.StylesTextView_style2,
                R.style.funTextGrayMedium16
            )
        }
        enforceFocus()
        mayUpdate = true
    }

    private fun update() {
        if (mayUpdate) {
            SpannableStringBuilder().run {
                if (text0.isNotEmpty()) append(buildSpannable(text0, style0))
                if (text1.isNotEmpty()) append(buildSpannable(" $text1", style1))
                if (text2.isNotEmpty()) append(buildSpannable(" $text2", style2))
                if (this.isNotEmpty()) setText(this, SPANNABLE)
            }
        }
    }
}
