package component

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity.*
import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast.LENGTH_SHORT
import android.widget.Toast.makeText
import android.widget.Toolbar
import extension.*
import kotlinfun.component.R

fun View.newToolbar(left: Any, titleText: Any, right: Any = EMPTY_STRING, onRightActionClick: View.() -> Unit = {}) =
    LinearToolbar(context).apply {
        title.text = stringAny(titleText)
        leftIcon.run {
            text = stringAny(left)
            if (isNotEmptyOrSuppressed) {
                backButtonContentDesc
                onClick { onBackPressed() }
            }
        }
        rightIcon.run {
            text = stringAny(right)
            if (isNotEmptyOrSuppressed) {
                backButtonContentDesc
                onClick(onRightActionClick)
            }
        }
        this setID titleText
    }

class LinearToolbar @JvmOverloads constructor(context: Context, set: AttributeSet? = null, defStyleAttr: Int = 0) :
    LinearLayout(context, set, defStyleAttr) {

    private val toolbarSizeDP = dpToPx(fun_BAR_SIZE)
    private val iconWidthDP = dpToPx(DEFAULT_ICON_WIDTH)

    val leftIcon: TextView = newText(R.style.funIconPurple24, padding = 8).apply {
        linearParams(iconWidthDP, toolbarSizeDP).gravity = START or CENTER_VERTICAL
        gravity = CENTER
        addBackgroundRipple()
        enforceFocus(true)
    }

    val title: TextView = newText(R.style.funTextBlack16, padding = 0).apply {
        linearParams(0, toolbarSizeDP).weight = 1f
        setPaddingDP(DEFAULT_PADDING, 0, 0, 0)
        gravity = CENTER
        enforceFocus(true)
    }

    val rightIcon: TextView = newText(R.style.funIconPurple24, padding = 8).apply {
        linearParams(iconWidthDP, toolbarSizeDP).gravity = END or CENTER_VERTICAL
        gravity = CENTER
        addBackgroundRipple()
        enforceFocus(true)
    }

    var leftIconColor: Int = R.color.black
        set(value) {
            field = value
            leftIcon.setTextColor(value)
        }

    var rightIconColor: Int = R.color.black
        set(value) {
            field = value
            rightIcon.setTextColor(value)
        }

    var textColor: Int = R.color.black
        set(value) {
            field = value
            title.setTextColor(value)
        }

    init {
        enforceFocus(false)
        this viewAs Toolbar::class
        setHeaderAccessibility()
        orientation = HORIZONTAL
        gravity = CENTER
        linearParams(MATCH_PARENT, toolbarSizeDP)
        obtainStyledAttributes(set, R.styleable.MenuButton) {
            title.text = getString(R.styleable.MenuButton_text) ?: EMPTY_STRING
            leftIcon.run {
                text = getString(R.styleable.MenuButton_left_icon) ?: EMPTY_STRING
                if (text.isNotEmpty()) setOnClickListener { onBackPressed() }
            }
            rightIcon.run {
                text = getString(R.styleable.MenuButton_right_icon) ?: EMPTY_STRING
                if (text.isNotEmpty()) setOnClickListener { makeText(context, title.text, LENGTH_SHORT).show() }
            }
            leftIconColor = getColor(R.styleable.MenuButton_left_icon_color, getColor(R.color.orange_500))
            rightIconColor = getColor(R.styleable.MenuButton_left_icon_color, getColor(R.color.orange_500))
            textColor = getColor(R.styleable.MenuButton_text_color, getColor(R.color.black))
        }
        background = getDrawable(R.drawable.cornered_white)
        addViews(leftIcon, title, rightIcon)
    }
}